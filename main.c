
// memory parts stack,text,bss,data,heap

// automatic variables
// program code 
// static and global without initialization
// static and global with initialization
// allocated memory

#include <stdlib.h>
char z; //bss
double f = 3.14; //data

int main(){ //text
    int a; //stack
    int b = 0; //stack
    int *p = &a; //stack
    //stack heap
    int* g = malloc(sizeof(int));
    free(g);
    static float h; //bss
    static float j = 2.0f; //data
}
